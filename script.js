const button = document.querySelector('#themeButton');
const body = document.body;

let isDarkTheme = false;

button.addEventListener('click', () => {
    if (isDarkTheme) {
        body.classList.remove('dark-theme');
        isDarkTheme = false;
    } else {
        body.classList.add('dark-theme');
        isDarkTheme = true;
    }
});


if (localStorage.getItem('darkTheme') === 'true') {
    body.classList.add('dark-theme');
    isDarkTheme = true;
}


button.addEventListener('click', () => {
    if (isDarkTheme) {
        localStorage.setItem('darkTheme', 'true');
    } else {
        localStorage.removeItem('darkTheme');
    }
});